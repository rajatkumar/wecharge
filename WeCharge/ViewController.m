//
//  ViewController.m
//  WeCharge
//
//  Created by Rajat on 1/11/15.
//  Copyright (c) 2015 codetastic. All rights reserved.
//

#import "ViewController.h"
#import <Parse/Parse.h>

#import "ParkViewController.h"
#import "PortCarViewController.h"

#define userNum 1
//#define userNum 2

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *carNameLabel;
@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	NSString *userName;
	if(userNum == 1)
	{
		userName = @"Tao";
	}
	else
	{
		userName = @"Rajat";
	}
	
	self.carNameLabel.text = [NSString stringWithFormat:@"%@'s i3", userName];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden{
	return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if([segue.identifier isEqualToString:@"find"])
	{
	ParkViewController *pvc = [segue destinationViewController];
	pvc.delegate = self;
	}
}

-(void)showPort{
			UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"
																 bundle:nil];
			PortCarViewController *pvc = [storyboard instantiateViewControllerWithIdentifier:@"PortCarViewController"];
			[self presentViewController:pvc animated:YES completion:nil];
}

@end
