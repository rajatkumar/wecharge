//
//  HistoryViewController.m
//  WeCharge
//
//  Created by Rajat on 1/11/15.
//  Copyright (c) 2015 codetastic. All rights reserved.
//

#import "HistoryViewController.h"

@interface HistoryViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSMutableArray *movers, *moveTimes;
@end

@implementation HistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	self.movers = [NSMutableArray arrayWithArray:@[@"Bruce Wayne", @"Clark Kent", @"Tony Stark"]];
	self.moveTimes = [NSMutableArray arrayWithArray:@[@"10 minutes ago", @"2 days ago", @"1 week ago"]];
	
	[self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeTapped:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
		return self.movers.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

	UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"hcell" forIndexPath:indexPath];
	cell.textLabel.text = self.movers[indexPath.row];
	cell.detailTextLabel.text = [NSString stringWithFormat:@"moved your car %@", self.moveTimes[indexPath.row]];
	
	return cell;
}
@end
