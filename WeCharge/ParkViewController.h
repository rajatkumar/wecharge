//
//  ParkViewController.h
//  WeCharge
//
//  Created by Rajat on 1/11/15.
//  Copyright (c) 2015 codetastic. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol portProtocol <NSObject>

-(void)showPort;

@end

@interface ParkViewController : UIViewController

@property (weak, nonatomic) id delegate;

@end
