//
//  ParkViewController.m
//  WeCharge
//
//  Created by Rajat on 1/11/15.
//  Copyright (c) 2015 codetastic. All rights reserved.
//

#import "ParkViewController.h"

#import "PortCarViewController.h"

@interface ParkViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSMutableArray *pending, *pendingTimes, *done, *doneTimes;
@end

@implementation ParkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	self.pending = [NSMutableArray arrayWithArray:@[@"Navdy Station #CP2", @"Navdy Station #CP3"]];
	self.pendingTimes = [NSMutableArray arrayWithArray:@[@"45 minutes", @"4 hours"]];
	
	self.done = [NSMutableArray arrayWithArray:@[@"Navdy Station #CP1"]];
	self.doneTimes = [NSMutableArray arrayWithArray:@[@"10 minutes", @"25 minutes", @"2 hours"]];
	
	[self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeTapped:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	return 2;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	return @[@"Available Now", @"Available Later"][section];
//	return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	return 45;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	if(section == 0)
		return self.done.count;
	else
		return self.pending.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"hcell" forIndexPath:indexPath];
	
	UIButton *button = (UIButton *)[cell.contentView viewWithTag:3];
	[button addTarget:self action:@selector(moveCarTapped) forControlEvents:UIControlEventTouchUpInside];
	button.hidden = indexPath.section == 1;
	UIImageView *buttonbg = (UIImageView *)[cell.contentView viewWithTag:6];
	buttonbg.layer.cornerRadius = 20.0;
	buttonbg.layer.masksToBounds = YES;
	buttonbg.hidden = indexPath.section == 1;
	
	UILabel *label1 = (UILabel *)[cell.contentView viewWithTag:1];
	UILabel *label2 = (UILabel *)[cell.contentView viewWithTag:2];
	
	UIImageView *marker = (UIImageView *)[cell.contentView viewWithTag:5];
	marker.layer.cornerRadius = 10.0;
	marker.layer.masksToBounds = YES;
	
	if(indexPath.section == 0){
		label1.text = self.done[indexPath.row];
		label2.text = @"Vehicle 100% charged";
		marker.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.5];
	}
	else
	{
		label1.text = self.pending[indexPath.row];
		label2.text = [NSString stringWithFormat:@"Free in %@", self.pendingTimes[indexPath.row]];
		marker.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.7];
	}
	return cell;
}

-(void)moveCarTapped{
	NSLog(@"move tapped");
	[self dismissViewControllerAnimated:YES completion:^{
//		UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"
//															 bundle:nil];
//		PortCarViewController *pvc = [storyboard instantiateViewControllerWithIdentifier:@"PortCarViewController"];
//		[self presentViewController:pvc animated:YES completion:nil];
		
		[self.delegate showPort];
	}];
}

- (IBAction)subscribeTapped:(id)sender {
	[self closeTapped:nil];
}


@end
