class SimulatorObject
{
  Animation image;
  int direction;
  int stepSize;
  boolean hasDirection;

  SimulatorObject(int canvasX, int canvasY, int posiX, int posiY,int imageWidth, int imageHeight, String imageName, int direction, int stepSize,boolean hasDirection, int batteryLevel, int count)
  {
    image = new Animation(posiX, posiY, imageWidth,imageHeight, imageName,"png", count, batteryLevel);
    this.direction = direction;
    this.stepSize = stepSize;
    this.hasDirection = hasDirection;
  }

  void moveLeft()
  {
    if(hasDirection && direction != 180)
    {
      direction = 180;
    }
    else
    {
      image.posiX -= stepSize;
    }
  }

  void moveRight()
  {
    if(hasDirection && direction != 0)
    {
      direction = 0;
    }
    else
    {
      image.posiX += stepSize;
    }
  }

  void moveUp()
  {
    if(hasDirection && direction != 270)
    {
      direction = 270;
    }
    else
    {
      image.posiY -= stepSize;
    }
  }

  void moveDown()
  {
    if(hasDirection && direction != 90)
    {
      direction = 90;
    }
    else
    {
      image.posiY += stepSize;
    }
  }

  void drawElement()
  {
    image.display(direction);
  }
  
  boolean intersect(SimulatorObject o)
  {
    return (image.posiX <= o.image.posiX+o.image.imageWidth &&
          o.image.posiX <= image.posiX+image.imageWidth &&
          image.posiY <= o.image.posiY+o.image.imageHeight &&
          o.image.posiY<= image.posiY+image.imageHeight);
  }
}

