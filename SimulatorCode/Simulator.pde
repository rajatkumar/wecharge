import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.client.*;
import org.apache.http.client.methods.*;
import org.apache.http.entity.*;
import org.apache.http.*;
SimulatorObject c0;
ArrayList<SimulatorObject> cars;
ArrayList<SimulatorObject> explosions;
SimulatorObject ch1;
SimulatorObject ch2;
SimulatorObject p1;
SimulatorObject controllable;
SimulatorObject chlogo1;
SimulatorObject chlogo2;
Key up;
Key left;
Key right;
Key down;
Key space;
Key nextCar;
Key charge;

class Pair
{
  String k;
  String v;
  Pair(String k, String v)
  {
    this.k = k;
    this.v = v;
  }
}

void parseCloudFunction(String functionName)
{
  String url = "https://api.parse.com/1/functions/"+functionName;
  HttpClient hc = new DefaultHttpClient();
  HttpPost p = new HttpPost(url);
  JSONObject object = new JSONObject();
  try {
      p.setHeader("X-Parse-Application-Id","WbWuAQjiqQMVL7Ujz4bP0WQmLiOtUHt9oZK9RV5w");
      p.setHeader("X-Parse-REST-API-Key","KimE4i3FEpawPtdftKnN5RKdkTNLy5HwfAU5RKyq");
      p.setHeader("Content-type", "application/json");
      HttpResponse resp = hc.execute(p);
      println(resp);
  } catch (Exception ex) {
    println(ex);
  }
}

void setup()
{
  frameRate(60);
  size(1200,600);
  cars = new ArrayList<SimulatorObject>();
  explosions = new ArrayList<SimulatorObject>();
  c0 = new SimulatorObject(width,height,150,90,150,75,"i3_",270,10,true,100,1);
  cars.add(c0);
  ch1 = new SimulatorObject(width,height,170,20,20,80,"chargepoint_",0,10,true,-1,1);
  ch2 = new SimulatorObject(width,height,280,20,20,80,"chargepoint_",0,10,true,-1,1);
  chlogo1 = new SimulatorObject(width,height,205,40,40,40,"chargepoint_logo_",0,10,true,-1,4);
  chlogo2 = new SimulatorObject(width,height,315,40,40,40,"chargepoint_logo_",0,10,true,-1,4);
  p1 = new SimulatorObject(width,height,200,200,25,50,"p_",0,10,false,-1,1);
  controllable = c0;
  left = new Key(37,"<");
  up = new Key(38,"^");
  right = new Key(39,">");
  down = new Key(40,"v");
  space = new Key(32,"");
  nextCar = new Key(90, "");
  charge = new Key(88,"");
}

void draw()
{
  drawBackground();
  ch1.drawElement();
  ch2.drawElement();
  chlogo1.drawElement();
  chlogo2.drawElement();
  for(int i = 0;i<explosions.size();i++)
  {
    explosions.get(i).drawElement();
  }
  for(int i = 0;i<cars.size();i++)
  {
    cars.get(i).drawElement();
  }
  if(p1 == controllable)
  {
    p1.drawElement();
  }
}

void keyPressed() {
  switch(keyCode)
  {
    case 67: //c
      if(p1 != controllable)
      {
        explosions.add(new SimulatorObject(width,height,controllable.image.posiX-controllable.image.imageWidth/4,controllable.image.posiY-controllable.image.imageHeight/4,200,200,"explosion_",0,10,false,-1,1));
        SimulatorObject newCar = new SimulatorObject(width,height,10,height+200,150,75,"i3_",270,10,true, getRandomBatteryLevel(),1);
        cars.add(newCar);
        cars.remove(controllable);
        controllable = newCar;
      }
      break;
    case 32://Space
      if(p1 == controllable)
      {
        for(int i = 0;i<cars.size();i++)
        {
          if(p1.intersect(cars.get(i)))
          {
            controllable = cars.get(i);
          }
        }
      }else 
      {
         p1.image.posiX = controllable.image.posiX;
         p1.image.posiY = controllable.image.posiY;
         if(controllable.intersect(ch1))
         {
           parseCloudFunction("parkedStationOne");
         }else if(controllable.intersect(ch2))
         {
           parseCloudFunction("parkedStationTwo");
         }
         controllable = p1;
      }
      space.count = 0;
      break;
    case 90://z
      SimulatorObject newCar = new SimulatorObject(width,height,10,height+200,150,75,"i3_",270,10,true, getRandomBatteryLevel(),1);
      cars.add(newCar);
      controllable = newCar;
      nextCar.count = 0;
      
     for(int i = 0;i<cars.size();i++)
     {
       if(ch1.intersect(cars.get(i)))
       {
         if(cars.get(i).image.batteryLevel == 100)
         {
           parseCloudFunction("carAvailable");
           return;
         }
       }else if(ch2.intersect(cars.get(i)))
       {
         if(cars.get(i).image.batteryLevel == 100)
         {
           parseCloudFunction("carAvailable");
           return;
         }
       }
     }
      
      break;
    case 88: //x
     charge.count = 0;
     for(int i = 0;i<cars.size();i++)
     {
       if(ch1.intersect(cars.get(i)))
       {
         cars.get(i).image.batteryLevel = 100;
       }else if(ch2.intersect(cars.get(i)))
       {
         cars.get(i).image.batteryLevel = 100;
       }
     }
     parseCloudFunction("carAvailable");
     break; 
    case 37:
      controllable.moveLeft();
      left.count = 0;
      break;
    case 38:
      controllable.moveUp();
      up.count = 0;
      break;
    case 39:
      controllable.moveRight();
      right.count = 0;
      break;
    case 40:
      controllable.moveDown();
      down.count = 0;
      break;
  }
  
}

int getRandomBatteryLevel()
{
  return (int)random(25,75);
}

void drawBackground()
{
  pushMatrix();
  stroke(255);
  fill(255);
  int h = height;
  int w = width*3/4;
  int hUnit = h/12;
  int wUnit = w/16;
  int halfLineWidth = 2;
  int r = 6;
  background(200,200,200);
  rect(wUnit*3-halfLineWidth,hUnit, halfLineWidth*2, hUnit*3, r, r, r, r);
  rect(wUnit*5-halfLineWidth,hUnit, halfLineWidth*2, hUnit*3, r, r, r, r);
  rect(wUnit*7-halfLineWidth,hUnit, halfLineWidth*2, hUnit*3, r, r, r, r);
  rect(wUnit*9-halfLineWidth,hUnit, halfLineWidth*2, hUnit*3, r, r, r, r);
  
  rect(wUnit*3-halfLineWidth,hUnit*8, halfLineWidth*2, hUnit*3, r, r, r, r);
  rect(wUnit*5-halfLineWidth,hUnit*8, halfLineWidth*2, hUnit*3, r, r, r, r);
  rect(wUnit*7-halfLineWidth,hUnit*8, halfLineWidth*2, hUnit*3, r, r, r, r);
  rect(wUnit*9-halfLineWidth,hUnit*8, halfLineWidth*2, hUnit*3, r, r, r, r);
  
  textSize(35);
  text("01",wUnit*3.5+halfLineWidth,hUnit*3.5);
  text("02",wUnit*5.5+halfLineWidth,hUnit*3.5);
  text("03",wUnit*7.5+halfLineWidth,hUnit*3.5);
  
  text("04",wUnit*3.5+halfLineWidth,hUnit*9);
  text("05",wUnit*5.5+halfLineWidth,hUnit*9);
  text("06",wUnit*7.5+halfLineWidth,hUnit*9);
  popMatrix();
  
  //Draw building
  fill(150,150,150);
  rect(wUnit*10.25,hUnit*1,wUnit*4, hUnit*5,6,6,6,6);
  fill(255);
  text("Navdy", wUnit*11.25,hUnit*3.5);
  text("Office", wUnit*11.25,hUnit*4.25);
  // Draw dashboard 
  fill(100,100,100);
  rect(wUnit*15,-10,wUnit*10,hUnit*20);
  fill(244,244,244);
  rect(wUnit*16,hUnit*1.5,wUnit*4.3, hUnit*8.5,16,16,16,16);
  
  // Draw buttons
  int keySpace = 6;
  left.drawElement((int)(wUnit*16.5),hUnit*8, wUnit, wUnit);
  down.drawElement((int)(wUnit*17.5)+keySpace,hUnit*8, wUnit, wUnit);
  right.drawElement((int)(wUnit*18.5)+keySpace*2,hUnit*8, wUnit, wUnit);
  up.drawElement((int)(wUnit*17.5)+keySpace,hUnit*8-wUnit-keySpace, wUnit, wUnit);
  nextCar.drawElement((int)(wUnit*16.5)+keySpace*2, hUnit*8-(int)(wUnit*2.5), wUnit*3-keySpace, wUnit);
  text("Next Car", (int)(wUnit*16.5)+keySpace*7, hUnit*8-(int)(wUnit*2.5)+keySpace*2, wUnit*3-keySpace, wUnit);
  space.drawElement((int)(wUnit*16.5)+keySpace*2, hUnit*8-(int)(wUnit*3.75), wUnit*3-keySpace, wUnit);
  text("Enter/Leave", (int)(wUnit*16.5)+keySpace*4, hUnit*8-(int)(wUnit*3.75)+keySpace*2, wUnit*3-keySpace, wUnit);
  charge.drawElement((int)(wUnit*16.5)+keySpace*2, hUnit*8-(int)(wUnit*5), wUnit*3-keySpace, wUnit);
  text("Charge", (int)(wUnit*16.5)+keySpace*9, hUnit*8-(int)(wUnit*5)+keySpace*2, wUnit*3-keySpace, wUnit);
}

