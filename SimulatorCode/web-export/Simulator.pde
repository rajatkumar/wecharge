
SimulatorObject c1;
SimulatorObject c2;
SimulatorObject p1;
SimulatorObject controllable;
Key up;
Key left;
Key right;
Key down;
Key space;
boolean pVisible;
boolean c2Visible;
void setup()
{
  
  
  frameRate(60);
  size(1200,600);
  c1 = new SimulatorObject(width,height,0,height-100,150,75,"i3_",270,10,true);
  c2Visible = false;
  c2 = new SimulatorObject(width,height,0,height+500,150,75,"i3_",270,10,true);
  pVisible = false;
  p1 = new SimulatorObject(width,height,200,200,25,50,"p_",0,10,false);
  controllable = c1;
  left = new Key(37,"<");
  up = new Key(38,"^");
  right = new Key(39,">");
  down = new Key(40,"v");
  space = new Key(32,"");
}

void draw()
{
  drawBackground();
  c1.drawElement();
  if(pVisible)
  {
    p1.drawElement();
  }
  if(c2Visible)
  {
    c2.drawElement();
  }
  controllable.drawElement();
}

void keyPressed() {
  switch(keyCode)
  {
    case 32://Space
      if(controllable == c1 || controllable == c2)
      {
        if(!pVisible)
        {
          pVisible = true;
          p1.image.posiX = controllable.image.posiX;
          p1.image.posiY = controllable.image.posiY;
          controllable = p1;
        }
      }else if(controllable == p1)
      {
        if(p1.intersect(c1))
        {
          controllable = c1;
          pVisible = false;
        }else if(p1.intersect(c2))
        {
          controllable = c2;
          pVisible = false;
        }
      }
      break;
    case 90://z
      c2Visible = true;
      pVisible = false;
      controllable = c2;
      break;
    case 37:
      controllable.moveLeft();
      left.count = 0;
      break;
    case 38:
      controllable.moveUp();
      up.count = 0;
      break;
    case 39:
      controllable.moveRight();
      right.count = 0;
      break;
    case 40:
      controllable.moveDown();
      down.count = 0;
      break;
  }
  
}

void drawBackground()
{
  pushMatrix();
  stroke(255);
  fill(255);
  int h = height;
  int w = width*3/4;
  int hUnit = h/12;
  int wUnit = w/16;
  int halfLineWidth = 6;
  int r = 6;
  background(200,200,200);
  rect(wUnit*3-halfLineWidth,hUnit, halfLineWidth*2, hUnit*3, r, r, r, r);
  rect(wUnit*5-halfLineWidth,hUnit, halfLineWidth*2, hUnit*3, r, r, r, r);
  rect(wUnit*7-halfLineWidth,hUnit, halfLineWidth*2, hUnit*3, r, r, r, r);
  rect(wUnit*9-halfLineWidth,hUnit, halfLineWidth*2, hUnit*3, r, r, r, r);
  
  rect(wUnit*3-halfLineWidth,hUnit*8, halfLineWidth*2, hUnit*3, r, r, r, r);
  rect(wUnit*5-halfLineWidth,hUnit*8, halfLineWidth*2, hUnit*3, r, r, r, r);
  rect(wUnit*7-halfLineWidth,hUnit*8, halfLineWidth*2, hUnit*3, r, r, r, r);
  rect(wUnit*9-halfLineWidth,hUnit*8, halfLineWidth*2, hUnit*3, r, r, r, r);
  
  textSize(35);
  text("01",wUnit*3.5+halfLineWidth,hUnit*3.5);
  text("02",wUnit*5.5+halfLineWidth,hUnit*3.5);
  text("03",wUnit*7.5+halfLineWidth,hUnit*3.5);
  
  text("04",wUnit*3.5+halfLineWidth,hUnit*9);
  text("05",wUnit*5.5+halfLineWidth,hUnit*9);
  text("06",wUnit*7.5+halfLineWidth,hUnit*9);
  popMatrix();
  // Draw buttons
  left.drawElement((int)(wUnit*16.5),hUnit*8, wUnit, wUnit);
  down.drawElement((int)(wUnit*17.5),hUnit*8, wUnit, wUnit);
  right.drawElement((int)(wUnit*18.5),hUnit*8, wUnit, wUnit);
  up.drawElement((int)(wUnit*17.5),hUnit*8-wUnit, wUnit, wUnit);
}

/*
General animation class to display animation

 Author: Tao Qian
 */
class Animation
{
  int posiX;
  int posiY;
  int imageWidth;
  int imageHeight;
  PImage[] images;
  int currentImage;
  int framePerImage;
  int currentFrame;

  private Animation(int posiX, int posiY, int imageWidth, int imageHeight)
  {
    setPosition(posiX, posiY);
    this.imageWidth = imageWidth;
    this.imageHeight = imageHeight;
    currentImage = -1;
    framePerImage = 1;
    currentFrame = -1;
  }

  Animation(int posiX, int posiY, int imageWidth, int imageHeight, String prefix, String extension, int count)
  {
    this(posiX, posiY, imageWidth, imageHeight);
    images = new PImage[count];
    for (int i = 0;i<images.length;i++)
    {
      images[i] = loadImage(prefix+i+"."+extension);
    }
  }

  Animation(int posiX, int posiY, int imageWidth, int imageHeight, String[] imagePaths)
  {
    this(posiX, posiY, imageWidth, imageHeight);
    images = new PImage[imagePaths.length];
    for (int i = 0;i<this.images.length;i++)
    {
      images[i] = loadImage(imagePaths[i]);
    }
  }

  Animation(int posiX, int posiY, int imageWidth, String prefix, String extension, int count)
  {
    this.posiX = posiX;
    this.posiY = posiY;
    this.imageWidth = imageWidth;
    currentImage = -1;
    framePerImage = 1;
    currentFrame = -1;
    images = new PImage[count];
    for (int i = 0;i<images.length;i++)
    {
      images[i] = loadImage(prefix+i+"."+extension);
    }
    imageHeight = (int)((float)images[0].height / (float)images[0].width * imageWidth);
  }


  void setPosition(int posiX, int posiY)
  {
    this.posiX = posiX;
    this.posiY = posiY;
  }

  void setFramePerImage(int framePerImage)
  {
    this.framePerImage = framePerImage;
    currentImage = 0;
  }

  void setCurrentImage(int currentImage)
  {
    this.currentImage = currentImage;
  }
  void setRepeating()
  {
    PImage[] newImages = new PImage[images.length*2-1];
    for (int i = 0;i< images.length;i++)
      newImages[i] = images[i];
    for (int i =images.length;i<newImages.length;i++)
      newImages[i] = images[newImages.length-i];
    images = newImages;
  }

  void display(int degree) {
    currentFrame = (currentFrame+1)%framePerImage;
    if (currentFrame == framePerImage -1 )
      currentImage = (currentImage+1)%images.length;
    pushMatrix();
    translate(posiX+imageWidth/2,posiY+imageHeight/2);
    rotate(radians(degree));
    image(images[currentImage], 0-imageWidth/2, 0-imageHeight/2, imageWidth, imageHeight);
    popMatrix();
  }
  
  void display() {
    display(180);
  }
}

class Key
{
  int code;
  String s;
  int count;
  
  Key(int code, String s)
  {
    this.code = code;
    this.s = s;
    count = 0;
  }
  
  void drawElement(int posiX, int posiY, int w, int h)
  {
    pushMatrix();
    int r = 6;
    if(keyCode == code && count >= 0)
    {
      count++;
    }
    if(count > 0 && count < 5)
    {
      fill(100);
      stroke(100);
    }else 
    {
      fill(255);
      stroke(255);
    }
    if(count == 5)
    {
      count = -1;
    }
    rect(posiX, posiY, w, h,r,r,r,r);
    fill(150);
    textSize(10);
    text(s,posiX+w/2,posiY+h/2);
    popMatrix();
  }
}
class SimulatorObject
{
  Animation image;
  int direction;
  int stepSize;
  boolean hasDirection;
  
  SimulatorObject(int canvasX, int canvasY, int posiX, int posiY,int imageWidth, int imageHeight, String imageName, int direction, int stepSize,boolean hasDirection)
  {
    image = new Animation(posiX, posiY, imageWidth,imageHeight, imageName,"png", 1);
    this.direction = direction;
    this.stepSize = stepSize;
    this.hasDirection = hasDirection;
  }

  void moveLeft()
  {
    if(hasDirection && direction != 180)
    {
      direction = 180;
    }
    else
    {
      image.posiX -= stepSize;
    }
  }

  void moveRight()
  {
    if(hasDirection && direction != 0)
    {
      direction = 0;
    }
    else
    {
      image.posiX += stepSize;
    }
  }

  void moveUp()
  {
    if(hasDirection && direction != 270)
    {
      direction = 270;
    }
    else
    {
      image.posiY -= stepSize;
    }
  }

  void moveDown()
  {
    if(hasDirection && direction != 90)
    {
      direction = 90;
    }
    else
    {
      image.posiY += stepSize;
    }
  }

  void drawElement()
  {
    image.display(direction);
  }
  
  boolean intersect(SimulatorObject o)
  {
    return (image.posiX <= o.image.posiX+o.image.imageWidth &&
          o.image.posiX <= image.posiX+image.imageWidth &&
          image.posiY <= o.image.posiY+o.image.imageHeight &&
          o.image.posiY<= image.posiY+image.imageHeight);
  }
}


