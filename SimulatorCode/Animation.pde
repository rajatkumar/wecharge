/*
General animation class to display animation

 Author: Tao Qian
 */
class Animation
{
  int posiX;
  int posiY;
  int imageWidth;
  int imageHeight;
  PImage[] images;
  int currentImage;
  int framePerImage;
  int currentFrame;
  int batteryLevel;
  int r;

  private Animation(int posiX, int posiY, int imageWidth, int imageHeight)
  {
    r = (int)random(0,6);
    setPosition(posiX, posiY);
    this.imageWidth = imageWidth;
    this.imageHeight = imageHeight;
    currentImage = 0;
    framePerImage = 6;
    currentFrame = -1;
  }

  Animation(int posiX, int posiY, int imageWidth, int imageHeight, String prefix, String extension, int count, int batteryLevel)
  {
    this(posiX, posiY, imageWidth, imageHeight);
    images = new PImage[count];
    for (int i = 0;i<images.length;i++)
    {
      images[i] = loadImage(prefix+i+"."+extension);
    }
    this.batteryLevel = batteryLevel;
  }

  Animation(int posiX, int posiY, int imageWidth, int imageHeight, String[] imagePaths)
  {
    this(posiX, posiY, imageWidth, imageHeight);
    images = new PImage[imagePaths.length];
    for (int i = 0;i<this.images.length;i++)
    {
      images[i] = loadImage(imagePaths[i]);
    }
  }

  Animation(int posiX, int posiY, int imageWidth, String prefix, String extension, int count)
  {
    this.posiX = posiX;
    this.posiY = posiY;
    this.imageWidth = imageWidth;
    currentImage = -1;
    framePerImage = 1;
    currentFrame = -1;
    images = new PImage[count];
    for (int i = 0;i<images.length;i++)
    {
      images[i] = loadImage(prefix+i+"."+extension);
    }
    imageHeight = (int)((float)images[0].height / (float)images[0].width * imageWidth);
  }


  void setPosition(int posiX, int posiY)
  {
    this.posiX = posiX;
    this.posiY = posiY;
  }

  void setFramePerImage(int framePerImage)
  {
    this.framePerImage = framePerImage;
    currentImage = 0;
  }

  void setCurrentImage(int currentImage)
  {
    this.currentImage = currentImage;
  }
  void setRepeating()
  {
    PImage[] newImages = new PImage[images.length*2-1];
    for (int i = 0;i< images.length;i++)
      newImages[i] = images[i];
    for (int i =images.length;i<newImages.length;i++)
      newImages[i] = images[newImages.length-i];
    images = newImages;
  }

  void display(int degree) {
    currentFrame = (currentFrame+1)%framePerImage;
    if (currentFrame == framePerImage -1 )
      currentImage = (currentImage+1)%images.length;
    pushMatrix();
    pushStyle();
    translate(posiX+imageWidth/2,posiY+imageHeight/2);
    rotate(radians(degree));
    if(batteryLevel >= 0)
    {
      if(r == 0)
        tint(200, 200, 220, 220);
      else if(r == 1)
        tint(200, 220, 200, 220);
      else if (r == 2)
        tint(220, 200, 200, 220);
      else if (r == 3)
        tint(230, 230, 180, 220);
      else if (r == 4)
        tint(230, 180, 230, 220);
      else if (r == 5)
        tint(180, 230, 230, 220);
    }
    image(images[currentImage], 0-imageWidth/2, 0-imageHeight/2, imageWidth, imageHeight);
    if(batteryLevel >= 0)
    {
      int portion = 255*batteryLevel/100;
      fill(255-portion,portion,0);
      noStroke();
      rect(0-imageWidth/6, 0-imageHeight/6, imageWidth/3*batteryLevel/100,imageHeight/3);
      stroke(200);
      strokeWeight(3);
      noFill();
      rect(0-imageWidth/6, 0-imageHeight/6, imageWidth/3,imageHeight/3);
      fill(200);
      rect(0-imageWidth/6+imageWidth/3, 0-imageHeight/6+imageWidth/18, imageWidth/36,imageHeight/9);
    }
    popMatrix();
    popStyle();
  }
  
  void display() {
    display(180);
  }
}

