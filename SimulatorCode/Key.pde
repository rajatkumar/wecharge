class Key
{
  int code;
  String s;
  int count;
  
  Key(int code, String s)
  {
    this.code = code;
    this.s = s;
    count = 0;
  }
  
  void drawElement(int posiX, int posiY, int w, int h)
  {
    pushMatrix();
    int r = 6;
    if(keyCode == code && count >= 0)
    {
      count++;
    }
    int textFill = 255;
    if(count > 0 && count < 5)
    {
      fill(100);
      stroke(100);
    }else 
    {
      fill(255);
      stroke(255);
      textFill = 100;
    }
    if(count == 5)
    {
      count = -1;
    }
    rect(posiX, posiY, w, h,r,r,r,r);
    fill(textFill);
    textSize(25);
    text(s,posiX+w/10*4,posiY+h/8*5);
    popMatrix();
  }
}
